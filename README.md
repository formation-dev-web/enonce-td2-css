CSS − TD2 : Le positionnement
=============================

Exo 1
-----

Intégrez la page d'accueil en respectant [le design suivant](td2-exo1.png) (design fixe, en pixels) ; vous devez produire :

- un fichier HTML5
- un fichier CSS

Utilisez le **positionnement absolu** pour placer vos colonnes ;
`inline-block` et `float` sont nécessaires pour d'autres éléments de la page.

### Aides :

- Réfléchissez à proposer une police de
  substitution pertinente à Verdana.
- Utilisez la règle CSS pour mesurer marges et blocs sur l'image (sous Firefox,
  il faut l'activer dans les options des outils de développement)
- Nous exprimerons pour ce TD toutes les dimensions en pixels (*px*)
- N'oubliez pas d'utiliser les balises sémantiques HTML5
- Les images nécessaires sont dans `img/`
- Validez CSS et  HTML

Exo 2 (BONUS)
-------------

Remplacez l'image du premier article par une vidéo (balise `<video>`) ; prenez
garde au(x) format(s) pour être compatible ; votre vidéo doit occuper le même emplacement sur la page que l'image et conserver la même légende.

- IE >= 9
- Chrome >= 40
- Safari >= 8
- Firefox >= 30

### Aides

- Des vidéos d'exemple en différents formats peuvent-être trouvées [sur le site quirksmode](http://www.quirksmode.org/html5/videos/).
- Pensez à la « dégradation gracieuse » (/graceful degradation/) pour les
  navigateurs plus anciens.
